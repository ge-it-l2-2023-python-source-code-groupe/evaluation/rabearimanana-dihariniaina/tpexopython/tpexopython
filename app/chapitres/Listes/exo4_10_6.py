#4.10.5 List & indice

def ListRange():
    print("En utilisant la liste et indice:")
    lstVide, lstFlottant = list(), [0,0]*5

    # Affichage de la liste vide 
    print(lstVide)

    # Affichage de la liste Flottante 
    print(lstFlottant)

    # Ajout de la liste vide les nombre entre 0 et 1000 avec 200 pas
    lstVide += range (0, 1001, 200)
    print(lstVide)

    # Affichage des entiers grace à Range
    #  Les entiers de 0 à 3 ;
    print(f"Les entiers de 0 à 3= {list(range(4))}")

    # Les entiers de 4 à 7 ;
    print(f"Les entiers de 4 à 7 = {list(range(4,8))}")

    # Les entiers de 2 à 8 par pas de 2.
    print(f"Les entiers de 2 à 8 par pas de 2 = {list(range(2,9,2))}")

    # Définir lstElmnt comme une liste des entiers de 0 à 5.
    lstElmnt = list(range(6))
    print(f"La nouvelle liste lstElmnt est {lstElmnt}")

    # Ajouter le contenu des deux listes() à la fin de la liste lstElmnt.
    lstElmnt += lstVide + lstFlottant
    print(f"La nouvelle où nous avons ajouté les deux listes est: {lstElmnt}")


# ListRange()

