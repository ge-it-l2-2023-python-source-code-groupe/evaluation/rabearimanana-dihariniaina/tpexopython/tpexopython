#Saisons

def Saison(): 

    # Listes des 4 saisons 
    hiver =["December","January","February"]
    printemps = ["March", "April", "May"]
    ete = ["June", "July", "August"]
    automne = ["September", "October", "November"]

    print(f"Les 4 listes de saisons sont \n Hiver: {hiver} \n Printempes: {printemps} \n Ete:{ete} \n Automne: {automne}")

    # La liste saison
    # Saisons = hiver + printemps + ete + automne
    Saisons = [hiver , printemps , ete , automne]
    print(f"La liste Saison est {Saisons}")

    # Prédiction: 
    print(f"Saisons[2]: {Saisons[2]}")
    print(f"Saisons[1][0]: {Saisons[1][0]}")
    print(f"Saisons[1:2]: {Saisons[1:2]}")
    print(f"Saisons[:][1]: {Saisons[:][1]}")
    print(f"Le dernier résultat Saisons[:][1] désigne le tout et puis sélectionné la liste à l'indice 1")
    

# Affichage
# Saison()