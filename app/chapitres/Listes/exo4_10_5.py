#4.10.5 List & indice
 

def ListIndice():
    Semaine = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"]
    #Afficher la liste semaine
    print(f"1) La liste Semaine: \n {Semaine}\n")

    # Afficher la valeur de semaine[4]
    print(f"2) La valeur de Semaine[4]: {Semaine[4]}")

    #3- Échanger les valeurs de la première et de la dernière case de cette liste
    temp = Semaine[0]
    Semaine[0] = Semaine[6]
    Semaine[6] = temp 

    print(f"3) Échanger les valeurs de la première et de la dernière case de cette liste est {Semaine}")

    # 4- Afficher 12 fois la valeur du dernier élément de la liste
    print(f"4) Affichage 12 fois la valeur du dernier élément de la liste est {Semaine[6]*12}")

# Affichage 
# ListIndice()



