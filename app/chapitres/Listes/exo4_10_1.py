#Jours de la semaine

def List1():
    # Initiation de la liste
    Semaines = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"]
    print(f"1)Les 7 jours de la Semaine sont {Semaines}\n")

    # Récupération des 5 premiers jours de la semaine 
    Week = Semaines[:5]
    print(f"1_a) Les 5 jours de la Semaine sont {Week}")


    # Récupération du week-end 
    Weekend = Semaines[5:]
    print(f"1_b) Le weekend de la Semaine sont {Weekend}")

    # Autre moyen d'indiçage 
    Week1 = Semaines[:-2]
    Weekend1 = Semaines[-2:]
    print(f"2)Les 2 autres moyens avec les memes résultats de la Semaine sont \n Week1 = Semaines[:-2] : {Week1} \n Weekend1 = Semaines[-2:]: {Weekend1} \n")

    # Accéder au dernier jour de la semaine
    LastDay= Semaines[6]
    LastDay1 = Semaines[-1]
    print(f"3)Les 2 autres manières avec les memes résultats pour accéder au dernier jour de la Semaine sont \n LastDay= Semaines[6]: {LastDay} \n LastDay1 = Semaines[-1]: {LastDay1}\n")


    # Inversion des jours de la semaine en une commande 
    InverseDay = Semaines[::-1]
    print(f"\n4)La Semaine inversé est {InverseDay}")

# Affichage 
# List1()

