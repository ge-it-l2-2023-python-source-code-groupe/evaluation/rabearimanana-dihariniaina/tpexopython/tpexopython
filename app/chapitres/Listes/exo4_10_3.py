#4.10.3 Table de multiplication par 9

def Multiplication9():
    # la table de multiplication par 9 en une seule commande avec les instructions range() et list().
    Table = list(range(0,91,9))
    print(f"la table de multiplication par 9 en une seule commande avec les instructions range() et list() : list(range(0,91,9)) est \n {Table}")

# Affichage
# Multiplication9()