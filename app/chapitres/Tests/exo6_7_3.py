#6.7.3 Minimum d'une liste


def minListe():
    liste = [8, 4, 6, 1, 5]
    min = liste[0]
    for i in range(len(liste)-1):
        if (liste[i]>liste[i+1]):
            min = liste[i+1]
        else:
            min = liste[i]

    print(f"Le minimum de notre liste [8, 4, 6, 1, 5] est: {min}")


# minListe()
