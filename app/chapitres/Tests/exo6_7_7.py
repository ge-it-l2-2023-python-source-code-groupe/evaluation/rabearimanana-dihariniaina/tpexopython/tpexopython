#6.7.7 Conjecture de Syracuse (exercice +++)

def syracuse():
    n = int(input("Entrez un nombre de lignes(entier positif): "))
    syracuse = [n]
    trivial = []
    while len(syracuse) < 50:
        n = n//2 if n % 2 == 0 else n*3+1
        syracuse += [n]
        if n == 1:
            trivial = syracuse[-3:]
    print(syracuse)
    print(f"Oui,la conjoncture de syracuse est vérifiée!\nLes nombres qui constituent le cycle trivial sont: {trivial}")

# syracuse()