#6.7.4 Fréquence des acides aminés

def frequenceAcidesAmines():
    acidesAmines = ["A","R","A","W","W","A","W","A","R","W","W","R","A","G"]
    print(f"La liste des acides aminés: {acidesAmines}")
    countA,countW,countR,countG = 0,0,0,0

    for i in range(len(acidesAmines)):
            if acidesAmines[i]=="A":
                countA += 1
            
            elif acidesAmines[i]=="W":
                countW += 1
            
            elif acidesAmines[i]=="R":
                countR += 1
                
            else:
                countG += 1
    print(f"\nLe fréquence de chaque acide aminé est : \nAlanine: {countA} \nTryptophane: {countW} \nArgine: {countR} \nGlycine: {countG}")
    

# frequenceAcidesAmines()