#6.7.9 Détermination des nombres premiers inférieurs à 100 (exercice +++)

#Méthode 1 (peu optimale mais assez intuitive)
def nombresPremiers():
    limit = 100
    print(f"Méthode 1: \nLes nombres premiers < {limit} sont: ")
    #Boucle infini : nb,count = 2,0
    count = 0
    for num in range(2, limit + 1):
        if all(num % i != 0 for i in range(2, int(num ** 0.5) + 1)):
            count += 1
            print(num, end=" ")

    print(f"\nIl y a {count} nombres premiers entre 0 et 100.")
    print("*"*25)
    
    ##############################################################
    #Méthode 2 (plus optimale et plus rapide, mais un peu plus compliquée)
    print("-"*20)
    limit = 100
    print(f"Méthode 2: \nLes nombres premiers <{limit} sont: ")
    premier,count = [2],1
    for num in range(3, limit + 1):
        is_prime = True
        for prime in premier:
            if num % prime == 0:
                is_prime = False
                break
        if is_prime:
            count += 1
            premier.append(num)
            print(num, end=" ")

    print(f"\nIl y a {count} nombres premiers entre 0 et 100.")



# nombresPremiers()