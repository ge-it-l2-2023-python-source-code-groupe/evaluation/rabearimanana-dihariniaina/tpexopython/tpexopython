#6.7.6 Nombres pairs

def NombresPairs():
    nombres = list(range(0, 21))
    print(f"La liste de nombres de 0 à 20 sont: {nombres}\n")

    listePair = []
    listeImpair = []

    for i in nombres:
        if i % 2 == 0 and i <= 10:
            listePair.append(i)

        elif i % 2 != 0 and i > 10:
            listeImpair.append(i)

    stars = "*"
    print(f"{stars*30}")
    print(f"La liste des nombres pairs inférieurs ou égaux à 10 est: {listePair}")
    print(f"La liste des nombres impairs strictement supérieurs à 10 est: {listeImpair}")

# NombresPairs()