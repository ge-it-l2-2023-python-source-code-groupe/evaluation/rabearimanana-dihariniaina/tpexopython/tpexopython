#6.7.5 Notes et mention d'un étudiant

def notesMention():
    notes = [14,9,13,15,12]
    print(f"LEs notes sont: {notes}\n")
    print(f"La note minimale est: {min(notes)} \nLa note maximale est: {max(notes)}") 
    moyenne=0
    note=0
    somme=0
    while note<len(notes):
        somme += notes[note]
        note += 1
    moyenne=somme/len(notes )
    print(f"La moyenne est : {moyenne:.2f}")
    print("L'\etudiant a eu la:")
    if(moyenne >= 10 and moyenne < 12):
        print("Mention Passable!")
    elif(moyenne >= 12 and moyenne < 14):
        print("Mention Assez Bien!")
    elif(moyenne >= 14):
        print("Mention Bien!")


# notesMention()