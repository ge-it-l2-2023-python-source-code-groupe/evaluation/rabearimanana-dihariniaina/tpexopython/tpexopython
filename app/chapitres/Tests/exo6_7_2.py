#6.7.2 Séquence complémentaire d'un brin d'ADN

def SequenceADN ():
    ADN = ["A","C","G","T","T","A","G","C","T","A","A","C","G"]
    print(f"La séquence d'ADN est: {ADN}")
    for i in range(len(ADN)):
        
        if ADN[i] == "A":
            ADN[i] = "T"
        elif ADN[i] == "T":
            ADN[i] = "A"
        elif ADN[i] == "C":
            ADN[i] = "G"
        else:
            ADN[i] = "C"

        complementaire = [i for i in ADN] 

    print("********************************************************")
    print(f"Sa séquence complémentaire est: {complementaire}") 


#Affichage 
# SequenceADN()