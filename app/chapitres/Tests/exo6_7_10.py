#6.7.10 Recherche d'un nombre par dichotomie (exercice +++)

def rechercheDichotomique():
    print("Recherche d'un nombre par dichotomie\n")
    print("Pensez à un nombre entre 1 et 100.")

    min,max= 1, 100
    questions = 1

    while min<= max:
        dicho= (min+ max) // 2
        reponse = input(f"Est-ce que votre nombre est plus grand (+), plus petit (-), ou égal (=) à {dicho}? [+/ -/=]: \n ")

        if reponse == "+":
            min = dicho + 1
        elif reponse == "-":
            max = dicho - 1
        elif reponse == "=":
            print(f"J'ai trouvé en {questions} questions ! Bravo !")
            break
        else:
            print("Veuillez entrer un caractère valide (+, -, =).")

        questions += 1
        

# rechercheDichotomique()

