 #6.7.8 Attribution de la structure secondaire des acides aminés d'une protéine (exercice +++)
#PsiPhi 

def phiPsi():
    angles_phi_psi = [
    [48.6, 53.4], [-124.9, 156.7], [-66.2, -30.8],
    [-58.8, -43.1], [-73.9, -40.6], [-53.7, -37.5],
    [-80.6, -26.0], [-68.5, 135.0], [-64.9, -23.5],
    [-66.9, -45.5], [-69.6, -41.0], [-62.7, -37.5],
    [-68.2, -38.3], [-61.2, -49.1], [-59.7, -41.1]
    ]

    phi_ideal, psi_ideal = -57.0, -47.0
    tolerance = 30.0
    for i, (phi, psi) in enumerate(angles_phi_psi, start=0):
        if abs((phi-phi_ideal) <= tolerance and (abs(psi - psi_ideal) <= tolerance)):
            print(f"{i}: [{phi}, {psi}] est en hélice")
        else:
            print(f"{i}: [{phi}, {psi}] n'est pas en hélice")

    print("\nLa structure secondaire majoritaire des 15 acides aminés est en hélice \n")
            
# phiPsi()