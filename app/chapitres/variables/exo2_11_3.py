# 2.11.3 Prédire le résultat : opérations et conversions de types

def Predilection2():
    print("Les résultats des opérations suivants:\n")

    print(f"str(4)*int('3') est {str(4)*int('3')}")
    print(f"int('3') + float('3.2') est {int('3')+float('3.2')}")
    print(f"str(3)*float('3.2') est une erreur")
    print(f"str(3/4)*2 est une erreur")


# Affichage
# Predilection2()