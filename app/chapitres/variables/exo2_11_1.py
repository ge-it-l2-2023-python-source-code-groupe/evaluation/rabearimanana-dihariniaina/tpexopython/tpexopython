# Nombres de Friedman

def Friedman():
    print("Les nombres de Friedman sont: \n")
    print(f"L'expression 7+3^6 est {7+3**6} qui est nombre de Friedman")
    print(f"L'expression (3+4)^3 est {((3+4)**3)} qui est nombre de Friedman")
    print(f"L'expression 3^6-5 est {3**6-5} qui n'est pas un nombre de Friedman")
    print(f"L'expression (1+2^8)^5 est {(1+2**8)*5} qui est nombre de Friedman")
    print(f"L'expression (2+1^8)^7 est {(2+1**8)**7} qui est nombre de Friedman")

# Affichage
# Friedman()