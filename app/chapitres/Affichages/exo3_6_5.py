#Ecriture formatée 2


def EcritureFormatee2():
    print("Affichage à l'aide de print l'ecriture formatée 1\n")
    perc_GC = ((4500+2575)/14800)* 100
    print(f"Le pourcentage de GC est {int(perc_GC)}{' ':>12} % ")
    print(f"Le pourcentage de GC est {perc_GC:.1f}{' ':>10} % ")
    print(f"Le pourcentage de GC est {perc_GC:.2f}{' ':>9} % ")
    print(f"Le pourcentage de GC est {perc_GC:.3f}{' ':>8} % ")

# Affichage
# EcritureFormatee2()
