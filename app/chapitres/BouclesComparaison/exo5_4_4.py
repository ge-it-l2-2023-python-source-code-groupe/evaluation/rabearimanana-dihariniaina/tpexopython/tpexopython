#5.4.4 Nombres pairs et impairs

def PairImpair():
    print("Nombres pairs et impairs ")
    impairs = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21] 
    pairs = []

    for i in impairs:
        pairs.append(i+1)
    
    print(f"Les nombres impairs sont: {impairs}")
    print(f"Les nombres pairs sont: {pairs}")


# Affichage 
# PairImpair()