#5.4.14 Suite de Fibonacci (exercice +++)

def fibonacci():
    print("Suite de Fibonacci")
    fibo = [0, 1]
    ratios = []

    for i in range(2, 15):
        fibo.append(fibo[i-1] + fibo[i-2])
    
    print("Liste des", 15, "premiers termes de la suite de Fibonacci:")
    print(fibo)

    for i in range(2, len(fibo)):
        ratio = fibo[i] / fibo[i-1]
        ratios.append(ratio)
        print(f"Rapport entre l'élément {i+1} et l'élément {i}: {ratio:.2f}")

    if len(set(ratios)) == 1:
        print(f"Le rapport tend vers une constante : {ratios[0]}")
    else:
        print("Le rapport ne tend pas vers une constante")

    return fibo, ratios

# Utilisation de la fonction pour générer la suite de Fibonacci et vérifier les rapports
    fibo_sequence, ratios = fibonacci()
