#5.4.13 Sauts de puce 

import random

def SautDePuce():
    print("Sauts de puce")
    position = 0
    cible = 5
    sauts = 0

    while position != cible:
        saut = random.choice([-1, 1])
        position += saut
        sauts += 1
        print(f"Puce saute à l'emplacement : {position}")

    print(f"La puce a atteint l'emplacement final en {sauts} sauts")

# Appel de la fonction pour simuler le mouvement de la puce
# SautDePuce()
