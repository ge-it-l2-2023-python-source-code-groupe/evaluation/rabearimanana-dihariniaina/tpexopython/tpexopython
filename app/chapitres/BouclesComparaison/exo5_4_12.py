#5.4.12 Parcours de demi-matrice sans la diagonale (exercice

def demiMatrice(n):
    print(f"\nDemi-matrice sans la diagonale (en gris) pour une matrice {n}x{n} :")
    print("\n Ligne Colonnes")
    
    cases = 0
    
    for i in range(n):
        for j in range(n):
            if i != j and j < i:
                print(f"{i+1:>4d} {j+1:>4d}")
                cases += 1
    
    print(f"\nTaille de la matrice : {n}x{n}")
    print(f"Nombre total de cases parcourues : {cases}")

# Tester avec différentes dimensions de matrice
# demiMatrice(4)  # Matrice 4x4
