#5.4.10 Pyramide

def Pyramid(row):
    space = row -1 
    stars = 1 

    for i in range (row):
        print(" " * space, end ="")

        print("*" * stars)

        space -=1
        stars +=2

def PyramidBase():
    print("Notre pyramide avec 10 lignes")
    row = 10
    Pyramid(row)

def PyramidCustom():
    print("Notre pyramide personnalisé par l'Utilisateur")
    # Demander à l'utilisateur 
    reponse = input("Entrez un nombre de lignes (entier positif): ")
    row = int(reponse)
    Pyramid(row)

    Pyramid()

# Affichage 
#Pyramide de base avec 10 row
# PyramidBase()

#Pyramide entrer par l'User 
# PyramidCustom()