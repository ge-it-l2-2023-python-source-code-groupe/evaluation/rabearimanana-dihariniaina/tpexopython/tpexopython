#5.4.1 Boucles de base 

def Boucles():
    animaux = ["vache", " souris","levure", " bacterie"]

    #Affichage N°1 : avec la boucle for: 
    print("Affichage N°1 : avec la boucle FOR: ")
    for animal in animaux: 
        print(animal)

    # Affichage N°2 : avec la boucle while
    print("Affichage N°1 : avec la boucle WHILE: ")
    i = 0
    while i < len(animaux):
        print(animaux[i])
        i+=1

    # Affichage N°3 : avec range 
    print("Affichage N°1 : avec RANGE ")
    for animal in range(len(animaux)):
        print(animaux[animal])


# Affichage 
# Boucles()