#5.4.11 Parcours de matrice

def matrice(n):
    print("Notre parcours de matrice avec un nombre entré par l'USER")
    # n = int(input("Entrez les éléments:"))    
    print ("\n Ligne colonne pour la boucle For")

    for i in range(n):
        for j in range (n):
            print(f"{i+1:>4d}{j+1:>4d}")
            print ("-"*10)
    print("*"*25)


# matrice(2)
# matrice(3)
# matrice(5)
# matrice(10)



def matrice2(n):
    print("Notre parcours de matrice avec un nombre entré par l'USER")
    
    # n = int(input("Entrez les éléments:")) 
    print("\n Ligne Colonnes pour la boucle While")
    
    i = 0
    while i < n:
        j = 0
        while j < n:
            print(f"{i+1:>4d} {j+1:>4d}")
            print("-" * 10)
            j += 1
        i += 1


# matrice2(2)
# matrice2(3)
# matrice2(5)
# matrice2(10)
