# 5.4.2 Boucle et jours de la semaine

def Semaine3(): 
    Semaine = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"]

    # Jour de la semaine avec la boucle For 
    print("Affichage avec la boucle FOR: ")
    print("Les jours de la semaine sont:")
    for i in range (len(Semaine)):
        print(Semaine[i])

    # Jours du weekend avec la boucle While 
    print("\nLes jours du week-end avec la boucle WHILE sont:")
    i = len(Semaine) - 2
    while i<len(Semaine):
        print(Semaine[i])
        i +=1

# Affichage 
# Semaine3()