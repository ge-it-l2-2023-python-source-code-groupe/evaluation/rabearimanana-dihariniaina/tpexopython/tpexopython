#Avec une boucle, affichez les nombres de 1 à 10 sur une seule ligne.

def Nombre10():
    print("Affichage des nombres de 1 à 10 avec la boucle FOR and RANGE ")
    for i in range(1, 11):
        print(i, end=",")

# Affichage 
# Nombre10()