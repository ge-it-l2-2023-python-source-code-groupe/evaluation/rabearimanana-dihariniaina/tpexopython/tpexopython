#5.4.6 Produit de nombres consécutifs

def NombresConsecutifs():
    entiers = list (range(2, 21, 2))

    prod = 1 
    print("Le produit des nombres consécutifs deux à deux est :")
    for i in range(len(entiers)-1):
        prod = entiers[i] * entiers[i+1] 
        print(f"{prod}")


# NombresConsecutifs()

