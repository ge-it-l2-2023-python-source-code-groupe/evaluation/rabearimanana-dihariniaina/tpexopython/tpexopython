from tools.console import clear
from pynput import keyboard

from chapitres.variables.exo2_11_1 import*
from chapitres.variables.exo2_11_2 import*
from chapitres.variables.exo2_11_3 import*
from chapitres.Affichages.exo3_6_1 import *
from chapitres.Affichages.exo3_6_2 import *
from chapitres.Affichages.exo3_6_3 import *
from chapitres.Affichages.exo3_6_4 import *
from chapitres.Affichages.exo3_6_5 import *
from chapitres.Listes.exo4_10_1 import *
from chapitres.Listes.exo4_10_2 import *
from chapitres.Listes.exo4_10_3 import * 
from chapitres.Listes.exo4_10_4 import * 
from chapitres.Listes.exo4_10_5 import * 
from chapitres.Listes.exo4_10_6 import *
from chapitres.BouclesComparaison.exo5_4_1 import *
from chapitres.BouclesComparaison.exo5_4_2 import *
from chapitres.BouclesComparaison.exo5_4_3 import * 
from chapitres.BouclesComparaison.exo5_4_4 import *
from chapitres.BouclesComparaison.exo5_4_5 import *
from chapitres.BouclesComparaison.exo5_4_6 import *
from chapitres.BouclesComparaison.exo5_4_7 import *
from chapitres.BouclesComparaison.exo5_4_8 import *
from chapitres.BouclesComparaison.exo5_4_9 import * 
from chapitres.BouclesComparaison.exo5_4_10 import *
from chapitres.BouclesComparaison.exo5_4_11 import *
from chapitres.BouclesComparaison.exo5_4_12 import *
from chapitres.BouclesComparaison.exo5_4_13 import *
from chapitres.BouclesComparaison.exo5_4_14 import *
from chapitres.Tests.exo6_7_1 import *
from chapitres.Tests.exo6_7_2 import *
from chapitres.Tests.exo6_7_3 import *
from chapitres.Tests.exo6_7_4 import *
from chapitres.Tests.exo6_7_5 import *
from chapitres.Tests.exo6_7_6 import * 
from chapitres.Tests.exo6_7_7 import * 
from chapitres.Tests.exo6_7_8 import *
from chapitres.Tests.exo6_7_9 import *
from chapitres.Tests.exo6_7_10 import *

def on_press(key):
    if key == keyboard.Key.enter:
        return False
def key():
     with keyboard.Listener(on_press=on_press) as Listener:
                  print("\nAPPUYEZ SUR ENTER POUR REVENIR ")
                  Listener.join()
                  clear()
def Menu():
    print("""
    BIENVENUE SUR MON APPLICATION DE TP \n
    ~~~By RABEARIMANANA Dihariniaina~~~

    ***********************************
          """)
    print(""" 
    :::::::::VOICI NOTRE MENU:::::::::::
    """)
    print("""
          1° Chapitre 1: Variables
          2° Chapitre 2: Affichage
          3° Chapitre 3: Listes
          4° Chapitre 4: Boucles et comparaison
          5° Chapitre 5: Tests
          6° Quitter
          """)
def chap1():
    print("CHAPITRE 1 : VARIABLE")
    print("""
         1) Nombre de Friedman
         2) Prédire le résultat : Opérations
         3) Prédire le résultat : Opérations et conversions de types
         4) Retour au Menu  
          """)   
def chap2():
    print("CHAPITRE 2: AFFICHAGE")
    print("""
    1)Permière affichage 
    2)Poly-A
    3)Poly-A et Poly_GC
    4)Ecriture Formatée 1
    5)Ecriture formatée 2
    6)Retour Menu
          """) 
def chap3():
    print("CHAPITRE 3: LISTE")
    print("""
    1)Semaine
    2)Saison
    3)Table de multiplicationde 9 (list ,range)
    4)Nombre de nombre pair entre [2,10000]
    5)Liste & Indice
    6)List & Range
    7)Retour Menu
          """) 
def chap4():
    print("CHAPITRE 4 : BOUCLE ET COMPARAISON ")
    print("""
    1)Boucle de base
    2)Boucle et jour de la semaine
    3)Nombre de 1 à 10 sur une ligne
    4)Nombre pairs et impairs
    5)Calcul de la moyenne
    6)Produit de nombre consécutifs
    7)Triangle
    8)Triangle inversé 
    9)Triangle gauche
    10)Pyramide
    11)Parcour de matrice
    12)Parcours de demi-maitrice sans la diagonale
    13) Saut de puce
    14)Suite de Fibonacci
    15)Retour au menu principale
          """) 
def chap5():
     print("CHAPITRE 5 : TESTS")
     print("""
        1)Jour de la semaine
        2)Séquence complémentaire d'un brin d'ADN
        3)Minimum d'un liste
        4)Fréquence d'acide aminés
        5)Notes et mention d'un étudiant
        6)Nombres pairs et impairs
        7)Conjecture de Syracuse
        8)Détermination des nombres premiers inférieurs à 100
        9) Hélice 
        10) Recherche d'un nombre par dichotomie
        11)Retour au menu principale
            """) 
        
    
"""_summary_
Endpoint 
"""
if __name__=="__main__":
    
  
    while True:
        Menu()
        choix=input("--Choisissez par un numéro:::> ")
        print("")
        clear()
        if choix == "1":
           while True:
               chap1()
               choix_1=input("====> ")
               print("")
               clear()
               if choix_1 =="1":
                   Friedman()
                   key()
                   continue
               elif choix_1=="2":
                   Predilection()
                   key()
                   continue
               elif choix_1=="3":
                   Predilection2()
                   key()
                   continue
               elif choix_1=="4":
                   print("===> Retour au Menu \n")
                   key()
                   break
            
        elif choix=="2":
            while True:
                chap2()
                choix_2=input("=====> ")
                print("")
                clear()
                if choix_2=="1":
                    Affichage()
                    key()
                    continue
                elif choix_2=="2":
                    PolyA()
                    key()
                    continue
                elif choix_2=="3":
                    PolyACG()
                    key()
                    continue
                elif choix_2=="4":
                    EcritureFormatee1()
                    key()
                    continue
                elif choix_2=="5":
                    EcritureFormatee2()
                    key()
                    continue
                elif choix_2=="6":
                    print("===> Retour au Menu \n ")
                    key()
                    break
        elif choix=="3":
            while True:
                chap3()
                choix_3=input("=====> ")
                print("")
                clear()
                if choix_3 =="1":
                    List1()
                    key()
                    continue
                elif choix_3=="2":
                    Saison()
                    key()
                    continue
                elif choix_3=="3":
                    Multiplication9()
                    key()
                    continue
                elif choix_3=="4":
                    Nombres1000()
                    key()
                    continue
                elif choix_3=="5":
                    ListIndice()
                    key()
                    continue
                elif choix_3=="6":
                    ListRange()
                    key()
                    continue
                elif choix_3=="7":
                    print("===> Retour au Menu \n")
                    key()
                    break
                    
        elif choix=="4":
            while True:
                chap4()
                choix_4=input("=====> ")
                print("")
                clear()
                if choix_4=="1":
                    Boucles()
                    key()
                    continue
                elif choix_4=="2":
                    Semaine3()
                    key()
                    continue
                elif choix_4=="3":
                    Nombre10()
                    key()
                    continue
                elif choix_4=="4":
                    PairImpair()
                    key()
                    continue
                elif choix_4=="5":
                    Moyenne()
                    key()
                    continue
                elif choix_4=="6":
                    NombresConsecutifs()
                    key()
                    continue
                elif choix_4=="7":
                    TriangleDroit()
                    key()
                    continue
                elif choix_4=="8":
                    TriangleInverse()
                    key()
                    continue
                elif choix_4=="9":
                    TriangleGauche()
                    key()
                    continue
                elif choix_4=="10":
                    PyramidBase()
                    key()
                    continue
                elif choix_4=="11":
                    n = int(input("Entrez les éléments:"))    
                    matrice(n)
                    matrice2(n)
                    key()
                    continue
                elif choix_4=="12":
                    n = int(input("Entrez les éléments:"))
                    demiMatrice(n)
                    key()
                    continue
                elif choix_4=="13":
                    SautDePuce()
                    key()
                    continue
                elif choix_4=="14":
                    fibonacci()
                    key()
                    continue
                elif choix_4=="15":
                    print("===> Retour au Menu \n ")
                    key()
                    break
        elif choix=="5":
            while True:
                chap5()
                choix_5=input("=====> ")
                print("")
                clear()
                if choix_5=="1":
                    JourDeSemaine()
                    key()
                    continue
                elif choix_5=="2":
                    SequenceADN()
                    key()
                    continue
                elif choix_5=="3":
                    minListe()
                    key()
                    continue
                elif choix_5=="4":
                    frequenceAcidesAmines()
                    key()
                    continue
                elif choix_5=="5":
                    notesMention()
                    key()
                    continue
                elif choix_5=="6":
                    PairImpair()
                    key()
                    continue
                elif choix_5=="7":
                    syracuse()
                    key()
                    continue
                elif choix_5=="8":
                    nombresPremiers()
                    key()
                    continue
                elif choix_5=="9":
                    phiPsi()
                    key()
                    continue
                elif choix_5=="10":
                    rechercheDichotomique()
                    key()
                    continue
                elif choix_5=="11":
                    print("===> Retour au Menu \n ")
                    key()
                    break
     
       
        elif choix=="6":
            print("""
            :::::::::::::::::::::MERCI D'AVOIR JOUER::::::::::::::::::::::::::::::
                  """)
            key()
            break
       
        
            
            
    
  
    
  
